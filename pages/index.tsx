import React, { useState } from "react";
import HeroBanner from "../components/HeroBanner";
import Player from "../components/Player";
import TextEditor from "../components/TextEditor";

const Home = () => {
  const [isPlayerDisplayed, setIsPlayerDisplayed] = useState<boolean>(false);
  const [textToRead, setTextToRead] = useState<string>("");

  const onPlayStopBtnClicked = (): void => {
    setIsPlayerDisplayed(!isPlayerDisplayed);
  };

  return (
    <>
      {!isPlayerDisplayed ? (
        <>
          <HeroBanner />
          <TextEditor
            textToRead={textToRead}
            onTextToReadChanged={(textToRead) => setTextToRead(textToRead)}
            onStartReadFastBtnClicked={onPlayStopBtnClicked}
          />
        </>
      ) : (
        <Player
          textToRead={textToRead}
          onExitBtnClicked={onPlayStopBtnClicked}
        />
      )}
    </>
  );
};

export default Home;
