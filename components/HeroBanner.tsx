import React from "react";
import commonStyles from "../styles/HeroBanner.module.scss";

const HeroBanner = () => {
  return (
    <div className={commonStyles.container}>
      <div className={commonStyles.titlesContainer}>
        <h1 className={commonStyles.title}>Faster Reader 🚀</h1>
        <h2 className={commonStyles.subTitle}>
          An application to help you, human, to read faster a text. The average
          adult reading speed for English prose text in the United States seems
          to be around 250 to 300 words per minute.
          <br />
          How fast will Faster Reader make you read ?
        </h2>
      </div>
    </div>
  );
};

export default HeroBanner;
